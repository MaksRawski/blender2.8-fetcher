if [ -d blender-2.80-* ]; then
	if [ "$(wget -q -O- https://builder.blender.org/download/ | grep -o -E "blender-2.80-.{0,50}"  | grep -o -E ".{0,30}-linux-glibc224-x86_64.tar.bz2" | cut -d "-" -f 3)" != "$(echo blender-2.80-* | cut -c 14-25 )" ]; then
		echo "Update available!"
	else
		echo "You already have the latest version!"
		exit 0
	fi
	rm -r blender-2.80-*
fi
wget --cut-dirs=2 -e robots=off -nH -q --show-progress -r --no-parent -R "index.html,robots.txt" -A "blender-2.80-*-linux-glibc224-x86_64.tar.bz2" https://builder.blender.org/download/

echo "Extracting..."
tar -xf blender-2.80-* && rm *.bz2
mv blender-2.80-* $(echo blender-2.80-* | cut -c 1-25)

echo "Creating shortcut in ~/.local/share/applications"
sed -i 's,Exec.*$,Exec='$(pwd)'/'$(echo blender-2.80-*)'/blender,' blender2.8.desktop
cp blender2.8.desktop ~/.local/share/applications/blender2.8.desktop
